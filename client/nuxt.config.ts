import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    modules: [
      '@pinia/nuxt',
    ],
    
    vite: {
      css: {
        preprocessorOptions: {
          scss: {
            additionalData: '@import "@/assets/style/main.scss";',
          },
        },
      },
    },
    // css: ['~/assets/style/main.scss', '~/assets/style/vars.scss'],
    publicRuntimeConfig: {
      apiURL: process.env.NODE_ENV === 'development' ? 'http://127.0.0.1:8000' : 'http://127.0.0.1:8000'
    },
})