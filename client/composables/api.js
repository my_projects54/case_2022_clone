export class ApiEndpoint {
  constructor(url, endpoint) {
    this.url = `${url}/api/${endpoint}`
  }

  async get(objectId = null, searchParams = null) {
    let url = this.url
    if (objectId !== null) {
      url += `${objectId}/`
    }
    if (searchParams !== null) {
      url += `?${new URLSearchParams(searchParams)}`
    }
    return fetch(url, {
      headers: {
        'Authorization': `Token ${window.localStorage.getItem('token')}`
      }
    }).then(response => response.json())
  }

  async post(data) {
    return fetch(this.url, {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json;',
        'Authorization': `Token ${window.localStorage.getItem('token')}`
      }
    })
  }

  async put(objectId, data) {
    let url = `${this.url}${objectId}/`
    return fetch(url, {
      method: 'put',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json;',
        'Authorization': `Token ${window.localStorage.getItem('token')}`
      }
    })
  }

  async delete(objectId) {
    let url = `${this.url}${objectId}/`
    return fetch(url, {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json;',
        'Authorization': `Token ${window.localStorage.getItem('token')}`
      }
    })
  }
}

export class UserApi extends ApiEndpoint {
  async getProfile() {
    let url = `${this.url}profile/`
    if (window.localStorage.getItem('token')){
      return fetch(url, {
        headers: {
          'Authorization': `Token ${window.localStorage.getItem('token')}`
        }
      }).then(response => response.json())
    }else{
      return null
    }
  }
  async login(form) {
    return fetch(this.url, {
      method: 'post',
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json;'
      }
    }).then(response=>response.json()).then(data=>localStorage.setItem('token', data.token))
  }
  async logout() {
    let url = `${this.url}logout/`
    if (window.localStorage.getItem('token')){
      return fetch(url, {
        headers: {
          'Authorization': `Token ${window.localStorage.getItem('token')}`
        }
      }).then(response => {
				localStorage.removeItem('token')
        return response.json()
      })
    }else{
      return null
    }
  }
}
// export const LoginApi = new ApiEndpointIsAuth('user/')
// export const GetAuthenticationUserApi = new ApiEndpointIsAuth('user/authenticate/')
// export const LogoutAuthenticationUserApi = new ApiEndpointIsAuth('user/logout/')