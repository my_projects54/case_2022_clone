from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from rest_framework.authtoken import views as rf_views
from api.routing import router

from core import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/user/', rf_views.obtain_auth_token),
    path('api/', include(router.urls)),
    path('filer/', include('filer.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)