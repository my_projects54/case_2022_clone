from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User, Profile


class ProfileTabularInline(admin.StackedInline):
    model = Profile
    extra = 1

@admin.register(User)
class UserAdmin(UserAdmin):
    fieldsets = (
        ('Данные пользователя', {
            'fields': (
                'email', 'username', 'password', 'is_staff', 'is_active', 'user_permissions', 'groups'
            )
        }),
    )
    inlines = [ProfileTabularInline]
