from rest_framework import serializers

from filer.models import File, Image

from .models import User, Profile


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('file', 'url', '_height', '_width')


class ProfileSerializer(serializers.ModelSerializer):
    photo = ImageSerializer()
    class Meta:
        model = Profile
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()
    class Meta:
        model = User
        fields = ['username', 'email', 'profile', 'is_staff']
        