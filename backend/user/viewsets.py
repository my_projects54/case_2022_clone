from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from django.contrib.auth.models import User

from .serializers import UserSerializer
# from core.models import Event

# class EventViewSet(viewsets.ModelViewSet):
#     serializer_class = EventSerializer

#     def get_queryset(self):
#         return Event.objects.all()

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.filter(is_active=1)

    @action(detail=False, methods=['get'])
    def profile(self, request):
        key = request.headers['Authorization'].split(' ')[1]
        token = Token.objects.filter(key=key).first()
        if token is not None:
            return Response(self.serializer_class(token.user).data)
        else:
            return Response(None)
        return Response(status=404)

    @action(detail=False, methods=['get'])
    def logout(self, request):
        key = request.headers['Authorization'].split(' ')[1]
        token = Token.objects.filter(key=key).first()
        if token is not None:
            token.delete()
            return Response(status=200, data='Вы успешно вышли')
        return Response(status=404, data='Пользователь не авторизован')