from django.db import models
from django.contrib.auth.models import AbstractUser

from filer.fields.image import FilerImageField


class User(AbstractUser):
    username = models.CharField(
        verbose_name='ФИО',
        max_length=150,
    )
    email = models.EmailField('email', unique=True)

    last_name = None
    first_name = None

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']


class Position(models.Model):
    name = models.CharField('Наменование должности', max_length=255)
    
    class Meta:
        verbose_name = 'должность'
        verbose_name_plural = 'Должности'
    
    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField('User', on_delete=models.CASCADE)
    photo = FilerImageField(on_delete=models.CASCADE, verbose_name='Аватарка')
    position = models.ForeignKey(Position, on_delete=models.SET_NULL, null=True, blank=True)
    phone = models.CharField(max_length=20, verbose_name='Телефон')
    
    class Meta:
        verbose_name = 'профиль'
        verbose_name_plural = 'Должности'
    
    def __str__(self):
        return self.user.username
