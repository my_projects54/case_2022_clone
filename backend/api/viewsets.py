from rest_framework import viewsets

from .serializers import TestSerializer, QuestionSerializer, QuestionItemSerializer
from core.models import Test, Question, QuestionItem


class TestViewSet(viewsets.ModelViewSet):
    serializer_class = TestSerializer

    def get_queryset(self):
        return Test.objects.all()
