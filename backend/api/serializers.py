from rest_framework import serializers

from core.models import Test, Question, QuestionItem


class QuestionItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionItem
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    question_item_true = QuestionItemSerializer(many=True)
    question_item_false = QuestionItemSerializer(many=True)
    class Meta:
        model = Question
        fields = '__all__'


class TestSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(source='get_questions', many=True)
    class Meta:
        model = Test
        fields = '__all__'