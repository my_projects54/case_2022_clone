from rest_framework import routers
from . import viewsets
from user import viewsets as UserViewsets

router = routers.DefaultRouter()
router.register('test', viewsets.TestViewSet, basename='test')
router.register('user', UserViewsets.UserViewSet, basename='user')