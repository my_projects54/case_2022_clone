from cgi import test
from distutils.version import Version
from django.contrib import admin

from .models import Test, Question, QuestionItem

class QuestionInLine(admin.StackedInline):
    model = Question
    filter_horizontal = ['question_item_true', 'question_item_false']
    extra = 0


@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    inlines = [QuestionInLine]


@admin.register(QuestionItem)
class QuestionItemAdmin(admin.ModelAdmin):
    pass