from tabnanny import verbose
from django.db import models
from filer.fields.image import FilerImageField


class QuestionItem(models.Model):
    text = models.CharField('Текст', max_length=512)

    class Meta:
        verbose_name = "вариант ответа"
        verbose_name_plural = "Варианты ответов"
    
    def __str__(self):
        return self.text


class Test(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    description = models.TextField('Короткое описание', max_length=512, null=True, blank=True)
    image = FilerImageField(on_delete=models.CASCADE, verbose_name='Изображение', null=True, blank=True)

    class Meta:
        verbose_name = "тест"
        verbose_name_plural = "Тесты"
    
    def __str__(self):
        return self.title

    def get_questions(self):
        return Question.objects.filter(test=self)


class Question(models.Model):
    test = models.ForeignKey('Test', verbose_name='Тест', on_delete=models.CASCADE)
    text = models.CharField('Текст вопроса', max_length=512)
    is_multiple = models.BooleanField('Несколько вариантов ответов', default=False)
    question_item_true = models.ManyToManyField(
        QuestionItem, 
        related_name='question_item_true',
        verbose_name='Правильные варианты ответов'
    ) 
    question_item_false = models.ManyToManyField(
        QuestionItem, 
        related_name='question_item_false',
        verbose_name='Не правильные варианты ответов'
    )

    class Meta:
        verbose_name = "вопрос"
        verbose_name_plural = "Вопросы"
    
    def __str__(self):
        return self.text