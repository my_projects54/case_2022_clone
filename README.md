ИНСТРУКЦИЯ РАЗВОРАЧИВАНИЯ ПРОЕКТА

СЕРВЕР
в паке backend
используется python 3.10.7, mysql 8.0

1) Создать виртуальное окружение
python -m venv .env

2) Включить виртуальное окружение
.env\Scripts\activate

(Выключить виртуальное окружение - deactivate)

3) Скачать пакеты библиотек в виртуальное окружение 
pip install -r requirements.txt

Записать список библиотек (pip freeze > requirements.txt)

4) В enser/local_settings.py указать подключение к БД (если не нужен проект в SQLite)
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_name',
        'USER': 'dn_user',
        'PASSWORD': 'db_password',
        'HOST': '127.0.0.1',
        'PORT': '3306'
    }
}

5) Выполнить миграции в бд
python manage.py migrate

6) Создать админа
python manage.py createsuperuser

7) Запустить сервер
python manage.py runserver


АВТОРИЗАЦИЯ
Запросы теперь принимаются только для авторизованных пользователей


для авторизации надо отправить POST запрос по url
/api/user/
с JSON телом например:

{"username":"admin","password":"admin"}


После чего вернется токен авторизации который необходимо вставить в заголовки запросов (ЗАПРОСЫ БЕЗ ЭТОГО ЗАГОЛОВКА РАБОТАТЬ НЕ БУДУТ), например

Authorization: Token 920fcdaasdfgxc2c70de12be745093d2095ecb82


для получения информации авторизованного пользователя надо отправить GET запрос на 
/api/user/profile/


а для выхода(удаление токена авторизации из бд) (GET запрос)

/api/user/logout/

после запроса на выход токен авторизации больше не действителен, не забыть убрать его из заголовков!